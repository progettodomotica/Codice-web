<html>

<head>
    <meta charset="utf-8" />
    <title>W.I.P</title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="funzioni.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Style/Material-light-themes/home-light-blue.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div id="menuLaterale">
    </div>
    <div id="coperturaPag"></div>
    <div id="topBar">
        <div id="contenutoTopBar">
            <div id="iconaMenu">
                <i class="material-icons" id="apriMenu">menu</i>
            </div>
            <div id="contenitoreLogo">
                <p id="testoLogo"></p>
            </div>
            <div id="iconacolori">
                <i class="material-icons" id="colore">palette</i>
            </div>
        </div>
    </div>
    <div id="menu"></div>
    <div id="main">
        <div id="boxContainer">
            <div class="box" id="box1">
                <div class="titoloBox">
                    <div class="testoTitolo"></div>
                    <div class="chiudiBox">
                        <i  id="c1" class="material-icons expand" onClick="slideUp(this.id)">expand_less</i>
                    </div>
                </div>
                <div id="b1" class="contenutoBox"></div>
            </div>
            <div class="box" id="box2">
                <div class="titoloBox">
                    <div class="testoTitolo"></div>
                    <div class="chiudiBox">
                        <i id="c2" class="material-icons expand">expand_less</i>
                    </div>
                </div>
                <div id="b2" class="contenutoBox"></div>
            </div>
            <div class="box" id="box3">
                <div class="titoloBox">
                    <div class="testoTitolo"></div>
                    <div class="chiudiBox">
                        <i  id="c3" class="material-icons expand">expand_less</i>
                    </div>
                </div>
                <div id="b3" class="contenutoBox"></div>
            </div>
            <div class="box" id="box4">
                <div class="titoloBox">
                    <div class="testoTitolo"></div>
                    <div class="chiudiBox">
                        <i id="c4" class="material-icons expand">expand_less</i>
                    </div>
                </div>
                <div id="b4" class="contenutoBox"></div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div id="contenutoFooter">
            <div id="testoFooter">
                <p id="pFooter">
                    Questo sito è stato realizzato da David Raul e Daniele Menotti. Lo scopo è quello di offrire un modo semplice ed intuitivo per controllare il mondo intorno a te.
                </p>
            </div>
        </div>
    </div>
    <script src="funzioni.js" type="text/javascript"></script>
</body>

</html>

<?php
     include 'client.php';
?>