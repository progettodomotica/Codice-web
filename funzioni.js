$(document).ready(function () {
    var nMenu = 0;
    var i = 0;
    var a = 0;
    var b = 0;
    var c = 0;

    function apriMenu() {
        $("#coperturaPag").css("display", "block");
        $("#menuLaterale").css("width", "20%");
        $("#apriMenu").text("clear");
    }

    function chiudiMenu() {
        //$("#coperturaPag").css("display", "none");
        $("#coperturaPag").fadeOut("500");
        $("#menuLaterale").css("width", "0%");
        $("#apriMenu").text("menu");
    }

    $("#c1").click(function () {
        if (i == 0) {
            $("#b1").css("height", "0px");
            $("#c1").text("expand_more");
            i = 1;
        } else {
            $("#b1").css("height", "250px");
            $("#c1").text("expand_less");
            i = 0;
        }

    });

    $("#c2").click(function () {
        if (a == 0) {
            $("#b2").css("height", "0px");
            $("#c2").text("expand_more");
            a = 1;
        } else {
            $("#b2").css("height", "250px");
            $("#c2").text("expand_less");
            a = 0;
        }

    });

    $("#c3").click(function () {
        if (b == 0) {
            $("#b3").css("height", "0px");
            $("#c3").text("expand_more");
            b = 1;
        } else {
            $("#b3").css("height", "250px");
            $("#c3").text("expand_less");
            b = 0;
        }

    });

    $("#c4").click(function () {
        if (c == 0) {
            $("#b4").css("height", "0px");
            $("#c4").text("expand_more");
            c = 1;
        } else {
            $("#b4").css("height", "250px");
            $("#c4").text("expand_less");
            c = 0;
        }

    });

    $("#coperturaPag").click(function () {
        chiudiMenu();
        nMenu = 0;
    });

    $("#apriMenu").click(function () {
        if (nMenu == 0) {
            apriMenu();
            nMenu = 1;
        } else {
            chiudiMenu();
            nMenu = 0;
        }

    });
});